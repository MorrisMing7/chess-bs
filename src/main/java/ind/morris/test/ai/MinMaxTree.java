package ind.morris.test.ai;

import java.util.ArrayList;
import java.util.List;

public class MinMaxTree {
    private Node root;
    private class Node{
        private List<Node> child;
        Decision decision;
        private State state;
        private Decision bestDecision;
        private Integer score;
        public Node(State s){
            this.state = s;
        }
        public Node(Decision d, State s){
            this.decision = d;
            this.state = s;
            this.score = s.getBow()?Integer.MIN_VALUE:Integer.MAX_VALUE;
        }
    }
    public interface Decision{ }
    public interface State{
        boolean getBow();
        int evaluate();
        State move(Decision d);
        List<Decision> choise();
    }
    public MinMaxTree(State s){
        this.root = new Node(s);
    }

    public Decision max(int depth){
        traverse(this.root,depth);
        return this.root.bestDecision;
    }

    private void traverse(Node node, int depth) {
        if(depth<0)
            return;
        if(node.child==null){
            node.child = new ArrayList<>();
            for(Decision d :node.state.choise()){
                Node n = new Node(d, node.state.move(d));
                node.child.add(n);
            }
        }
        if(depth==0){
            boolean bow = node.state.getBow();
            for(Node n :node.child){
                int s = n.state.evaluate();
                if(bow && s>node.score || !bow && s<node.score){
                    node.bestDecision = n.decision;
                    node.score = s;
                }
            }
            return;
        }
        for(Node d :node.child){
            traverse(d, depth-1);
        }
        for(Node n:node.child){
            boolean bow = node.state.getBow();
            if(bow && n.score>node.score || !bow && n.score<node.score){
                node.bestDecision = n.decision;
                node.score = n.score;
            }
        }
    }
    
}
