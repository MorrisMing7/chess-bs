package ind.morris.test.chess;

import lombok.Data;

@Data
public class Piece {
    String name;
    boolean bow;

    public Piece(String name, boolean bow) {
        this.name = name;
        this.bow = bow;
    }
}
