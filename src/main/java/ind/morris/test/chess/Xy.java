package ind.morris.test.chess;

public class Xy {
    public int x;
    public int y;

    public Xy(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Xy(Xy t) {
        this.x = t.x;
        this.y = t.y;
    }

    @Override
    public String toString() {
        return "{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof Xy){
            Xy other = (Xy) o;
            return this.x == other.x && this.y ==other.y;
        }
        return false;
    }
}
