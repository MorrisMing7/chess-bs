package ind.morris.test.chess;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class Util {

    public static final String pieceK = "\u2654";
    public static final String pieceQ = "\u2655";
    public static final String pieceR = "\u2656";
    public static final String pieceB = "\u2657";
    public static final String pieceN = "\u2658";
    public static final String pieceP = "\u2659";

//    //html
//    public static final String pieceK = "&#9812";
//    public static final String pieceQ = "&#9813";
//    public static final String pieceR = "&#9814";
//    public static final String pieceB = "&#9815";
//    public static final String pieceN = "&#9816";
//    public static final String pieceP = "&#9817";

//    //黑色样式
//    public static final String pieceK = "\u265A";
//    public static final String pieceQ = "\u265B";
//    public static final String pieceR = "\u265C";
//    public static final String pieceB = "\u265D";
//    public static final String pieceN = "\u265E";
//    public static final String pieceP = "\u265F";

    public static final String[] piece_order = new String[]{pieceR,pieceN,pieceB,pieceQ,pieceK,pieceB,pieceN,pieceR};
    private final ChessInfo chessInfo;

    public Util(ChessInfo chessInfo) {
        this.chessInfo = chessInfo;
    }

    public static ChessInfo newAGame(){

        final ChessInfo chessInfo = new ChessInfo();
        chessInfo.whoseTurn = false;

        for(int i=0;i<8;i++){
            chessInfo.board[i][0]=new Piece(piece_order[i],false);
            chessInfo.board[i][1]=new Piece(pieceP,false);
            chessInfo.board[i][7]=new Piece(piece_order[i],true);
            chessInfo.board[i][6]=new Piece(pieceP,true);
        }
        return chessInfo;
    }

    private List<OneMove> findPossibleMoveWhenCheck() {
        List<OneMove> result = new ArrayList<>();
        if(this.chessInfo.checkBowKing==null)
            throw new RuntimeException("没有被将军");
        final Xy from = this.chessInfo.checkBowKingMove.from;
        final Xy to = this.chessInfo.checkBowKingMove.to;
        final Piece[][] board = this.chessInfo.board;
        //将军的棋子到将的路径上所有的点
        List<Xy> toList = new ArrayList<>();
        if(!board[from.x][from.y].name.equals(pieceP)&&!board[from.x][from.y].name.equals(pieceN)){
            int dx=to.x-from.x;
            dx = Integer.compare(dx, 0);
            int dy=to.y-from.y;
            dy = Integer.compare(dy, 0);
            for(int x=from.x+dx, y=from.y+dy;
                x!=to.x||y!=to.y;
                x+=dx,y+=dy ){
                toList.add(new Xy(x,y));
            }
        }
        boolean bow = board[to.x][to.y].bow;  //被将一方
        //可以吃掉将军子的走法
        final List<Xy> list = findPieceCanMoveToXyExcludeKing(from.x, from.y, bow);
        for(Xy j:list){
            result.add(new OneMove(j,from));
        }
        //挡住的走法
        for(Xy i:toList){
            final List<Xy> list2 = findPieceCanMoveToXyExcludeKing(i.x, i.y, bow);
            for(Xy j:list2){
                result.add(new OneMove(j,i));
            }
        }
        //移动将的走法
        final Xy kingXy = this.chessInfo.kingBowXy[bow ? 1 : 0];
        final ArrayList<Xy> kingMove = new ArrayList<>();
        moveK(kingXy.x, kingXy.y, kingMove);
        for(Xy i:kingMove){
            result.add(new OneMove(to,i));
        }
        return result;
    }

    public BaseResp<List<String>> highlight(String clickPoint) {
        final Piece[][] board = chessInfo.board;
        Xy cXy = clickPoint2Xy(clickPoint);
        int x = cXy.x;
        int y = cXy.y;
        final Piece piece = board[x][y];
        if(piece==null){
            return new BaseResp<>("400","选择棋子以移动");
        }
        if(piece.bow!=chessInfo.whoseTurn){
            return new BaseResp<>("400","轮到"+(chessInfo.whoseTurn?"黑色":"白色"));
        }
        if(this.chessInfo.checkBowKing==null) {
            List<Xy> result = pieceCanMoveTo(x, y);        
            if(result.size()==0)
                return new BaseResp<>("400", "没有可走步");
            return new BaseResp<>(result.stream().map(t -> "" + ((char) ('A' + t.x)) + (t.y + 1)).collect(Collectors.toList()));
        }
        if(this.chessInfo.checkBowKing != chessInfo.whoseTurn){
            throw new RuntimeException("咋回事");
        }
        List<OneMove> result = findPossibleMoveWhenCheck();
        if(result.size()==0)
            return new BaseResp<>("400", "将死");
        final List<Xy> collect = result.stream().filter(m -> m.from.equals(cXy)).map(m -> m.to).collect(Collectors.toList());
        if(collect.size()==0)
            return new BaseResp<>("400", "没有可走步");
        return new BaseResp<>(collect.stream().map(t -> "" + ((char) ('A' + t.x)) + (t.y + 1)).collect(Collectors.toList()));
    }

    public List<Xy> pieceCanMoveTo(int x, int y){
        final Piece piece = chessInfo.board[x][y];
        List<Xy> result = new ArrayList<>();
        switch (piece.name){
            case pieceP:
                moveP(x,y,result);
                break;
            case pieceR:
                moveR(x,y,result);
                break;
            case pieceN:
                moveN(x,y,result);
                break;
            case pieceB:
                moveB(x,y,result);
                break;
            case pieceQ:
                moveQ(x,y,result);
                break;
            case pieceK:
                moveK(x,y,result);
                break;
            default:
                throw new RuntimeException("？？");
        }
        //检验移动后是否会被将军
        Piece[][] cache = this.chessInfo.board;     //备份
        Iterator<Xy> it = result.iterator();
        while(it.hasNext()){
            Xy newXy = it.next();
            //初始化
            this.chessInfo.board = new Piece[8][8];
            for(int i = 0;i<8;i++){
                System.arraycopy(cache[i], 0, this.chessInfo.board[i], 0, 8);
            }
            //移动棋子
            this.chessInfo.board[newXy.x][newXy.y] = this.chessInfo.board[x][y];
            this.chessInfo.board[x][y] = null;
            final Xy kingXy = this.chessInfo.kingBowXy[piece.bow ? 1 : 0];
            final List<Xy> l = threatenedBy(kingXy.x, kingXy.y, piece.bow);
            if(l.size()!=0){
                it.remove();
            }
        }
        this.chessInfo.board = cache;
        return result;
    }

    private void moveK(int x, int y, List<Xy> result) {
        boolean bow = this.chessInfo.board[x][y].bow;
        canMove(x+1,y+1,bow,result);
        canMove(x+1,y-1,bow,result);
        canMove(x-1,y+1,bow,result);
        canMove(x-1,y-1,bow,result);

        canMove(x,y+1,bow,result);
        canMove(x,y-1,bow,result);
        canMove(x-1,y,bow,result);
        canMove(x+1,y,bow,result);
        //去掉受威胁的走法
        result.removeIf(i -> canNotMove(i.x, i.y, bow));

        //王车易位
        if(bow && !this.chessInfo.KingHasMoved[1] || !bow && !this.chessInfo.KingHasMoved[0]){        //王没有移动过
            //王不能被将军
            if(this.chessInfo.checkBowKing!=null && this.chessInfo.checkBowKing==bow){
                return;
            }
            //车没有移动过
            if(!this.chessInfo.RookABowHasMoved[bow?1:0] &&
                //王车之间没有棋子
                this.chessInfo.board[1][y]==null && this.chessInfo.board[2][y]==null && this.chessInfo.board[3][y]==null &&
                //易位点与途径点没有受到威胁
                result.contains(new Xy(3,y)) && !canNotMove(2,y,bow) ){
                    result.add(new Xy(2,y));
            }
            //另一侧
            if(!this.chessInfo.RookHBowHasMoved[bow?1:0] &&
                this.chessInfo.board[5][y]==null && this.chessInfo.board[6][y]==null &&
                result.contains(new Xy(5,y)) && !canNotMove(6,y,bow) ){
                    result.add(new Xy(6,y));
            }
        }
    }

    /**
     * bow颜色的王是否**不可以**移动到xy点
     */
    private boolean canNotMove(int x, int y, boolean bow) {
        return threatenedBy(x,y,bow).size()!=0;
    }

    /**
     * 颜色为bow的xy处受到哪些威胁, 查找哪些非bow的棋子可以攻击此处
     */
    private List<Xy> threatenedBy(int x, int y, boolean bow){
        List<Xy> result = findPieceCanMoveToXyExcludeKP(x,y,!bow);
        Xy t;
        //受到敌方King的威胁
        t = pieceFind(x, y, 0, 1, !bow, false, pieceK);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, 0, -1, !bow, false, pieceK);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, -1, 0, !bow, false, pieceK);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, 1, 0, !bow, false, pieceK);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, 1, 1, !bow, false, pieceK);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, 1, -1, !bow, false, pieceK);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, -1, 1, !bow, false, pieceK);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, -1, -1, !bow, false, pieceK);
        if(t!=null) result.add(t);
        //收到Pawn的威胁
        if(bow){
            t = pieceFind(x,y,-1,-1,!bow,false,pieceP);
            if(t!=null) result.add(t);
            t = pieceFind(x,y,1,-1,!bow,false,pieceP);
            if(t!=null) result.add(t);
        }else {
            t = pieceFind(x,y,-1,1,!bow,false,pieceP);
            if(t!=null) result.add(t);
            t = pieceFind(x,y,1,1,!bow,false,pieceP);
            if(t!=null) result.add(t);
        }
        return result;
    }

    /**
     * 查找能够移动到xy点的bow颜色的棋子
     */
    private List<Xy> findPieceCanMoveToXyExcludeKing(int x, int y, boolean bow){
        List<Xy> result = findPieceCanMoveToXyExcludeKP(x,y,bow);
        Xy t = pieceFind(x,y,0,bow?1:-1,bow,false,pieceP);
        if(t!=null)
            result.add(t);
        return result;
    }
    private List<Xy> findPieceCanMoveToXyExcludeKP(int x, int y, boolean bow) {
        List<Xy> result = new ArrayList<>();
        Xy t;
        //Q R
        t = pieceFind(x, y, 0, 1, bow, true, pieceQ, pieceR);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, 0, -1, bow, true, pieceQ, pieceR);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, -1, 0, bow, true, pieceQ, pieceR);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, 1, 0, bow, true, pieceQ, pieceR);
        if(t!=null) result.add(t);
        //Q B
        t = pieceFind(x, y, 1, 1, bow, true, pieceQ, pieceB);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, 1, -1, bow, true, pieceQ, pieceB);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, -1, 1, bow, true, pieceQ, pieceB);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, -1, -1, bow, true, pieceQ, pieceB);
        if(t!=null) result.add(t);
        //N
        t = pieceFind(x, y, 1, 2, bow, false, pieceN);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, 1, -2, bow, false, pieceN);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, -1, 2, bow, false, pieceN);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, -1, -2, bow, false, pieceN);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, 2, 1, bow, false, pieceN);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, 2, -1, bow, false, pieceN);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, -2, 1, bow, false, pieceN);
        if(t!=null) result.add(t);
        t = pieceFind(x, y, -2, -1, bow, false, pieceN);
        if(t!=null) result.add(t);

        return result;
    }

    /**
     * 检查在 xy指示的位置的 dxdy方向上是否有 bow颜色的names棋子
     */
    private Xy pieceFind(int x, int y, int dx, int dy, boolean bow, boolean recursive,String...names) {
        x = x+dx;
        y = y+dy;
        if(!b07(x,y))
            return null;
        final Piece piece = this.chessInfo.board[x][y];
        if(piece !=null){
            if(piece.bow==bow) {
                for (String i : names) {
                    if (piece.name.equals(i)){
                        return new Xy(x,y);
                    }
                }
                return null;
            }else {
                return null;
            }
        }
        if(recursive)
            return pieceFind(x,y,dx,dy,bow,true,names);
        return null;
    }

    private void moveQ(int x, int y, List<Xy> result) {
        moveR(x,y,result);
        moveB(x,y,result);
    }

    private void moveB(int x, int y, List<Xy> result) {
        boolean bow = this.chessInfo.board[x][y].bow;
        for(int cx = x+1,cy = y+1; canMove(cx, cy,bow,result); cx +=1,cy+=1);
        for(int cx = x-1,cy = y-1; canMove(cx, cy,bow,result); cx -=1,cy-=1);
        for(int cx = x+1,cy = y-1; canMove(cx, cy,bow,result); cx+=1, cy -=1);
        for(int cx = x-1,cy = y+1; canMove(cx, cy,bow,result); cx-=1,cy +=1);
    }

    private void moveR(int x, int y, List<Xy> result) {
        boolean bow = this.chessInfo.board[x][y].bow;
        for(int cx = x+1; canMove(cx, y,bow,result); cx +=1);
        for(int cx = x-1; canMove(cx, y,bow,result); cx -=1);
        for(int cy = y+1; canMove(x,cy,bow,result); cy +=1);
        for(int cy = y-1; canMove(x,cy,bow,result); cy -=1);
    }

    private void moveN(int x, int y, List<Xy> result) {
        if(!b07(x,y))
            return;
        Piece[][] board = this.chessInfo.board;
        boolean bow = board[x][y].bow;
        canMove(x+1,y+2,bow,result);
        canMove(x+1,y-2,bow,result);
        canMove(x+2,y+1,bow,result);
        canMove(x+2,y-1,bow,result);
        canMove(x-1,y+2,bow,result);
        canMove(x-1,y-2,bow,result);
        canMove(x-2,y+1,bow,result);
        canMove(x-2,y-1,bow,result);
    }


    /**
     * 棋子是否可以移动到该点，可以则将坐标放入result
     * @param bow 该棋子的颜色
     * @return 是否需要递归找可移动点
     */
    private boolean canMove(int x, int y, boolean bow, List<Xy> result){
        if(!b07(x,y))
            return false;
        Piece[][] board = this.chessInfo.board;
        if(board[x][y]==null){
            result.add(new Xy(x,y));
            return true;
        }else if(board[x][y].bow!=bow) {
            result.add(new Xy(x,y));
        }
        return false;
    }

    private void moveP(int x, int y, List<Xy> result) {
        boolean bow = this.chessInfo.board[x][y].bow;
        int dy = bow?-1:1;
        Piece[][] board = this.chessInfo.board;
        if(b07(y+dy) && board[x][y+dy]==null){
            result.add(new Xy(x,y+dy));
        }
        //首次移动
        if(y==6&&bow || y==1&&!bow){
            if(b07(x,y+2*dy) && board[x][y+2*dy]==null && board[x][y+dy]==null){
                result.add(new Xy(x,y+2*dy));
            }
        }
        //过路兵
        if(y==4&&!bow || y==3&&bow){
            if(this.chessInfo.passByPawn!=null && Math.abs(this.chessInfo.passByPawn.x-x)==1){
                result.add(new Xy(this.chessInfo.passByPawn.x,y+dy));
            }
        }
        //吃子移动
        if(b07(x-1,y+dy) &&board[x-1][y+dy]!=null &&board[x-1][y+dy].bow!=bow){
            result.add(new Xy(x-1,y+dy));
        }
        if(b07(x+1,y+dy) &&board[x+1][y+dy]!=null &&board[x+1][y+dy].bow!=bow){
            result.add(new Xy(x+1,y+dy));
        }
    }

    private boolean b07(int t){
        return t>=0 && t<=7;
    }
    private boolean b07(int x,int y){
        return b07(x)&&b07(y);
    }
    private Xy clickPoint2Xy(String clickPoint){
        int x = clickPoint.charAt(0)-'A';
        int y = Integer.parseInt(clickPoint.substring(1))-1;
        return new Xy(x,y);
    }

    public BaseResp<ChessInfo> move( String pieceLoc, String newLoc) {
        Xy cXy = clickPoint2Xy(pieceLoc);
        Xy newXy = clickPoint2Xy(newLoc);
        final Piece[][] board = this.chessInfo.board;
        if(board[cXy.x][cXy.y].name.equals(pieceP)){
            //判断兵升变
            if(board[cXy.x][cXy.y].bow && newXy.y==0 ||
                !board[cXy.x][cXy.y].bow && newXy.y==7 ){
                this.chessInfo.pawnPromote = new OneMove(cXy,newXy);
                //直接返回，先进行升变
                return new BaseResp<>(this.chessInfo.dto());
            }else {
                this.chessInfo.pawnPromote = null;
            }
            //记录过路兵
            if((Math.abs(cXy.y- newXy.y)==2)){
                this.chessInfo.passByPawn = newXy;
            }
            //吃过路兵
            else if(this.chessInfo.passByPawn!=null && board[newXy.x][newXy.y]==null &&cXy.x!=newXy.x){
                board[this.chessInfo.passByPawn.x][this.chessInfo.passByPawn.y] = null;
                this.chessInfo.passByPawn = null;
            }else {
                this.chessInfo.passByPawn = null;
            }
        }else {
            this.chessInfo.passByPawn = null;
            this.chessInfo.pawnPromote = null;
        }
        //移动棋子
        board[newXy.x][newXy.y] = board[cXy.x][cXy.y];
        board[cXy.x][cXy.y] = null;
        //todo 升变

        final Piece piece = board[newXy.x][newXy.y];
        //该步是否将军
        checkIfCheck(!piece.bow);

        if(pieceR.equals(piece.name)){
            if(piece.bow&& cXy.y==7||!piece.bow&&cXy.y==0){
                if(cXy.x==0){
                    this.chessInfo.RookABowHasMoved[piece.bow?1:0] = true;
                }else if (cXy.x==7){
                    this.chessInfo.RookHBowHasMoved[piece.bow?1:0] = true;
                }
            }
        }else if(pieceK.equals(piece.name)){
            //王车易位
            if(!this.chessInfo.KingHasMoved[piece.bow?1:0] && Math.abs(newXy.x-cXy.x)>1){
                if(newXy.x-cXy.x>0){
                    board[5][cXy.y] = board[7][cXy.y];
                    board[7][cXy.y] = null;
                }else {
                    board[3][cXy.y] = board[0][cXy.y];
                    board[0][cXy.y] = null;
                }
            }
            this.chessInfo.kingBowXy[piece.bow?1:0] = newXy;
            this.chessInfo.KingHasMoved[piece.bow?1:0] = true;
        }
        this.chessInfo.whoseTurn = !this.chessInfo.whoseTurn;
        return new BaseResp<>(chessInfo.dto());
    }

    private void checkIfCheck(boolean bow) {
        final Xy enemyKing = this.chessInfo.kingBowXy[bow ? 1 : 0];
        final List<Xy> checkMove = threatenedBy(enemyKing.x, enemyKing.y, bow);
        if(checkMove.size()!=0) {
            this.chessInfo.checkBowKing = bow;
            this.chessInfo.checkBowKingMove = new OneMove(checkMove.get(0), enemyKing);
        }else{
            this.chessInfo.checkBowKing = null;
        }
    }

    public BaseResp<ChessInfo> pawnPromote(String name) {
        if(this.chessInfo.pawnPromote == null){
            return new BaseResp<>("400","不是升变状态");
        }
        Xy from = this.chessInfo.pawnPromote.from;
        Xy to = this.chessInfo.pawnPromote.to;
        this.chessInfo.board[to.x][to.y] = this.chessInfo.board[from.x][from.y];
        this.chessInfo.board[to.x][to.y].name = name;
        this.chessInfo.board[from.x][from.y] = null;
        this.chessInfo.whoseTurn = !this.chessInfo.whoseTurn;
        //检查升变后是否将军
        checkIfCheck(!this.chessInfo.board[to.x][to.y].bow);
        this.chessInfo.pawnPromote = null;
        return new BaseResp<>(chessInfo.dto());
    }
}
