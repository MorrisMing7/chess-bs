package ind.morris.test.chess;

import lombok.Data;

@Data
public class OneMove {
    Xy from;
    Xy to;

    public OneMove(Xy from, Xy to) {
        this.from = from;
        this.to = to;
    }

    public OneMove copy() {
        return new OneMove(new Xy(from),new Xy(to));
    }
}
