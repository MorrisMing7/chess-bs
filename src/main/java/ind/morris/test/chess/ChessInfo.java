package ind.morris.test.chess;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class ChessInfo {
    //上一步的过路兵
    Xy passByPawn;
    //兵升变标识
    OneMove pawnPromote;
    //黑白King是否在被将军
    Boolean checkBowKing;
    //表示将军的那一步
    OneMove checkBowKingMove;
    //将的位置
    @JsonIgnore
    Xy[] kingBowXy = new Xy[]{new Xy(4,0),new Xy(4,7)};
    //记录四个rook是否移动
    @JsonIgnore
    boolean[] RookABowHasMoved =new boolean[]{false,false}; //A侧的黑白rook是否移动过
    @JsonIgnore
    boolean[] RookHBowHasMoved =new boolean[]{false,false}; //H侧的黑白rook是否移动过
    //王是否移动过
    @JsonIgnore
    boolean[] KingHasMoved=new boolean[]{false,false};

    //black or white -> true or false
    Boolean whoseTurn;
    //使用形如 A2 D8 作为 xy
    Map<String,Piece> xy2Piece;
    @JsonIgnore
    Piece[][] board = new Piece[8][8];

    public ChessInfo dto(){
        xy2Piece = new HashMap<>();
        for(int i=0;i<8;i++){
            for(int j=0;j<8;j++){
                if(board[i][j]!=null){
                    char t = (char)('A' + i);
                    xy2Piece.put(""+ t +(j+1),board[i][j]);
                }
            }
        }
        return this;
    }

    ChessInfo copy() {
        ChessInfo c = new ChessInfo();
        c.passByPawn = new Xy(this.passByPawn);
        c.checkBowKing = checkBowKing;
        c.checkBowKingMove = this.checkBowKingMove.copy();

        c.kingBowXy = new Xy[2];
        c.RookABowHasMoved =new boolean[2];
        c.RookHBowHasMoved =new boolean[2];
        c.KingHasMoved=new boolean[2];

        System.arraycopy(this.kingBowXy,0,c.kingBowXy,0,2);
        System.arraycopy(this.RookABowHasMoved,0,c.RookABowHasMoved,0,2);
        System.arraycopy(this.RookHBowHasMoved,0,c.RookHBowHasMoved,0,2);
        System.arraycopy(this.KingHasMoved,0,c.KingHasMoved,0,2);
        c.whoseTurn = this.whoseTurn;
        c.board = new Piece[8][8];
        for(int i = 0;i<8;i++){
            System.arraycopy(this.board[i], 0, c.board[i], 0, 8);
        }
        return c;
    }
}
