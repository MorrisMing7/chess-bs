package ind.morris.test.chess;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping("/chess")
@CrossOrigin
public class ChessController {
    public static ConcurrentHashMap<String, ChessInfo> games = new ConcurrentHashMap<>();

    @GetMapping("/game/{id}")
    public BaseResp<ChessInfo> game(@PathVariable("id")String id){
        ChessInfo chessInfo=games.get(id);
        if(chessInfo==null){
            synchronized (id.intern()){
                chessInfo = games.get(id);
                if(chessInfo ==null){
                    chessInfo = Util.newAGame();
                    games.put(id,chessInfo);
                }
            }
        }
        return new BaseResp<>(chessInfo.dto());
    }

    @PutMapping("/game/{id}/highlight/{xy}")
    public BaseResp<List<String>> highlight(@PathVariable("id")String id,@PathVariable("xy")String clickPoint){
        ChessInfo chessInfo=games.get(id);
        if(chessInfo==null)
            return new BaseResp<>("404","没有该对局");
        return new Util(chessInfo).highlight(clickPoint);
    }
    @PostMapping("/game/{id}/move/{xy}/{xy2}")
    public BaseResp<ChessInfo> highligh1t(@PathVariable("id")String id,@PathVariable("xy")String pieceLoc,@PathVariable("xy2")String newLoc){
        ChessInfo chessInfo=games.get(id);
        if(chessInfo==null)
            return new BaseResp<>("404","没有该对局");
        return new Util(chessInfo).move(pieceLoc,newLoc);
    }
    @PostMapping("/game/{id}/pawnPromote/{name}")
    public BaseResp<ChessInfo> highligh1t(@PathVariable("id")String id,@PathVariable("name")String name){
        ChessInfo chessInfo=games.get(id);
        if(chessInfo==null)
            return new BaseResp<>("404","没有该对局");
        return new Util(chessInfo).pawnPromote(name);
    }
}
