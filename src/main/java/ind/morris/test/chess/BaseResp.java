package ind.morris.test.chess;

import lombok.Data;

@Data
public class BaseResp<T>{
    private T data;
    private String code;
    private String msg;

    public BaseResp(T data) {
        this.data = data;
        this.code = "200";
        this.msg = "ok";
    }

    public BaseResp(T data, String code, String msg) {
        this.data = data;
        this.code = code;
        this.msg = msg;
    }
    public BaseResp(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
